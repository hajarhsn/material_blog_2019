<?php
/*
    ./app/modeles/postsModele.php
*/
namespace App\Modeles\PostsModele;

function findAll(\PDO $connexion){
  $sql = "SELECT *, posts.id as postID
          FROM posts
          JOIN auteurs ON posts.auteur = auteurs.id
          ORDER BY datePublication DESC
          LIMIT 5;";

$rs = $connexion->query($sql);
return $rs->fetchAll(\PDO::FETCH_ASSOC);
  // le fetchAll signifie que je vais avoir
  // la liste des posts sous forme
  // d'un tableau indéxé en tableau associatif
}

/**
 * @param PDO $connexion [description]
 * @param int $id        [description]
 * @return array         [description]s
 */

function findOneById(\PDO $connexion, int $id):array {
  $sql = "SELECT *
          FROM posts
          JOIN auteurs ON posts.auteur = auteurs.id
          WHERE posts.id = :id;";
  $rs = $connexion->prepare($sql);
  $rs->bindValue(':id', $id, \PDO::PARAM_INT);
  $rs->execute();
  return $rs->fetch(\PDO::FETCH_ASSOC);
}
