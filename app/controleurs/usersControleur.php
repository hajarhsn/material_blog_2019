<?php
/*
    ./app/controleurs/usersControleur.php
*/
namespace App\Controleurs\UsersControleur;
use \App\Modeles\UsersModele AS User;

/**
 * [loginFormAction description]
 * @return [type] [description]
*/
 function loginFormAction() {
   // Je n'ai rien à aller chercher dans le modèle
   // Je charge la vue loginForm dans $content1
   GLOBAL $content1;
   ob_start();
     include '../app/vues/users/loginForm.php';
   $content1 = ob_get_clean();
 }

function loginVerificationAction(\PDO $connexion, array $data){
  // Je vais chercher le user qui correspond au login et au mot de passe
    include_once '../app/modeles/usersModele.php';
    $user = User\findOneByLoginAndPassword($connexion, $data);

  // Si j'ai trouvé un user... on le redirige vers le backoffice
  // Sinon, je le redirige vers le formulaire avec un message d'erreur
  if ($user):
    header('location: http://www.promotion-sociale.be');
  else:
    header('location: ' . ROOT . 'login?error=1');
  endif;
}
