<?php
/*
  ./app/vues/templates/defaut.php
*/
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include '../app/vues/templates/partials/_head.php'; ?>
</head>
<body>
  <!-- NAVIGATION -->
  <?php include '../app/vues/templates/partials/_nav.php'; ?>

  <?php include '../app/vues/templates/partials/_content.php'; ?>

  <!-- FOOTER -->
  <?php include '../app/vues/templates/partials/_footer.php'; ?>

  <!-- SCRIPTS -->
  <?php include '../app/vues/templates/partials/_script.php'; ?>
</body>
</html>
